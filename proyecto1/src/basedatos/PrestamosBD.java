/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basedatos;

import clases.BaseDatos;
import clases.Helpers;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author rorti
 */
public class PrestamosBD {
    private BaseDatos   bdInterfaz;
    private Connection conexion;
    private Helpers helper;
    
    public PrestamosBD() throws Exception{
        this.bdInterfaz =   new BaseDatos();
        this.conexion   =   bdInterfaz.conectarBaseDatos();
        this.helper     =   new Helpers();
    }
    
    public PrestamosBD(Connection conexion) {
        this.bdInterfaz =   new BaseDatos();
        this.conexion   =   conexion;
        this.helper     =   new Helpers();
    }
    
    public String[] consultarPrestamo(String tipoUsuario,String identificacion,String codigoLibro){
        ResultSet   resultSet   =   null;
        String[]    estudiantes =   new String[7];
        String query = "SELECT ID_PRESTAMO, LIBRO, TIPO_USUARIO, IDENTIFICACION, FECHA_PRESTAMO, FECHA_ENTREGA, ESTADO  "
                     + "FROM prestamo "
                     + "WHERE TIPO_USUARIO = '"+tipoUsuario+"' "
                     + "AND IDENTIFICACION = '"+identificacion+"' "
                     + "AND LIBRO = '"+codigoLibro+"' "
                     + "LIMIT 1";
        
        try{
            resultSet   =   bdInterfaz.consultaDinamicaRegistrosBaseDatos(getConexion(), query);
            
            if (resultSet.next()) {
                
                estudiantes[0]  =   resultSet.getInt("ID_PRESTAMO")+"";
                estudiantes[1]  =   resultSet.getString("LIBRO");
                estudiantes[2]  =   resultSet.getString("TIPO_USUARIO");
                estudiantes[3]  =   resultSet.getString("IDENTIFICACION");
                estudiantes[4]  =   resultSet.getString("FECHA_PRESTAMO");
                estudiantes[5]  =   resultSet.getString("FECHA_ENTREGA");
                estudiantes[6]  =   resultSet.getString("ESTADO");
            }else{
                estudiantes =   null;
            }
            
        }catch(Exception e){
            estudiantes =   null;
        }
        
        return estudiantes;
    }
    
    public ArrayList<String[]> obtenerlListaVencidasEstudiantes(){
        ArrayList<String[]> listaVencidas = new ArrayList<>();
        ResultSet   resultSet   =   null;
        String query = "SELECT  pr.LIBRO,lib.title,lib.author,est.IDENTIFICACION, CONCAT(est.NOMBRE,' ',est.APELLIDO) as nombrePrest,pr.FECHA_ENTREGA "
                     + " FROM prestamo pr "
                     + " JOIN estudiantes est ON "
                     + " est.identificacion = pr.IDENTIFICACION "
                     + " JOIN books lib ON "
                     + " lib.cod = pr.LIBRO "
                     + " WHERE  NOW() > pr.FECHA_ENTREGA "
                     + " AND pr.ESTADO = 'Prestado' "                     
                     + " AND pr.TIPO_USUARIO = 'estudiantes' "
                     + " LIMIT 1";
        
        try{
            resultSet   =   bdInterfaz.consultaDinamicaRegistrosBaseDatos(getConexion(), query);
            
            while (resultSet.next()) {
                String[] datos = new String[]{
                    resultSet.getString("LIBRO"),
                    resultSet.getString("title"),
                    resultSet.getString("author"),
                    resultSet.getString("IDENTIFICACION"),
                    resultSet.getString("nombrePrest"),
                    resultSet.getString("FECHA_ENTREGA")
                };
                
                listaVencidas.add(datos);
            }
            
        }catch(Exception e){
        }
        
        return listaVencidas;
    }
    
    public ArrayList<String[]> obtenerlListaVencidasPersonalAlbertino(){
        ArrayList<String[]> listaVencidas = new ArrayList<>();
        ResultSet   resultSet   =   null;
        String query = "SELECT  pr.LIBRO,lib.title,lib.author,est.identificacion, CONCAT(est.nombres,' ',est.apellidos) as nombrePrest,pr.FECHA_ENTREGA "
                     + " FROM prestamo pr "
                     + " JOIN usuarios est ON "
                     + " est.identificacion = pr.IDENTIFICACION "
                     + " JOIN books lib ON "
                     + " lib.cod = pr.LIBRO "
                     + " WHERE  NOW() > pr.FECHA_ENTREGA "
                     + " AND pr.ESTADO = 'Prestado' "
                     + " AND pr.TIPO_USUARIO = 'usuarios' "
                     + " LIMIT 1";
        
        try{
            resultSet   =   bdInterfaz.consultaDinamicaRegistrosBaseDatos(getConexion(), query);
            
            while (resultSet.next()) {
                String[] datos = new String[]{
                    resultSet.getString("LIBRO"),
                    resultSet.getString("title"),
                    resultSet.getString("author"),
                    resultSet.getString("IDENTIFICACION"),
                    resultSet.getString("nombrePrest"),
                    resultSet.getString("FECHA_ENTREGA")

                };
                
                listaVencidas.add(datos);
            }
            
        }catch(Exception e){
        }
        
        return listaVencidas;
    }
    
    public boolean actualizarEstadoPrestamo(int idPrestamo , String estado ){
        boolean resultado;
        String query = "UPDATE prestamo SET "
                + "ESTADO = '"+estado+"' "
                + "WHERE ID_PRESTAMO = '"+idPrestamo+"'";
        
       try{
            bdInterfaz.insertarRegistroBaseDatos(conexion, query);
            resultado   =   true;
        }catch(Exception e){
            resultado   =   false;
        }
        
        return resultado; 
    }
    
    public boolean guardarPrestamo(
             String tipoUsuario
            ,String identificacion
            ,String codigoLibro
            ,Date fechaPrestamo
            ,Date fechaEntrega
            ,String estado){
        
        boolean resultado   =   false;
        String query = "INSERT INTO prestamo "
                + "("
                + "  LIBRO"
                + ", TIPO_USUARIO"
                + ", IDENTIFICACION"
                + ", FECHA_PRESTAMO"
                + ", FECHA_ENTREGA"
                + ", ESTADO) " 
                + " VALUES ('"+codigoLibro+"'"
                + ",'"+tipoUsuario+"'"
                + ",'"+identificacion+"'"
                + ","+helper.obtenerFechaDB(fechaEntrega)+""
                + ","+helper.obtenerFechaDB(fechaPrestamo)+""
                + ",'"+estado+"') ";        
        try{
            bdInterfaz.insertarRegistroBaseDatos(conexion, query);
            resultado   =   true;
        }catch(Exception e){
            resultado   =   false;
        }
        
        return resultado;
    }
    
    public boolean actualizarPrestamo(
             String idPrestamo
            ,String tipoUsuario
            ,String identificacion
            ,String codigoLibro
            ,Date fechaPrestamo
            ,Date fechaEntrega
            ,String estado){
        
        boolean resultado   =   false;
        String query = "UPDATE prestamo SET"
                + "  LIBRO  =   '"+codigoLibro+"'"
                + ", TIPO_USUARIO   =   '"+tipoUsuario+"'"
                + ", IDENTIFICACION =   '"+identificacion+"'"
                + ", FECHA_PRESTAMO =   "+helper.obtenerFechaDB(fechaEntrega)
                + ", FECHA_ENTREGA  =   "+helper.obtenerFechaDB(fechaPrestamo)
                + ", ESTADO =   '"+estado+"'"
                + " WHERE ID_PRESTAMO   =   '"+idPrestamo+"' ";        
        try{
            bdInterfaz.insertarRegistroBaseDatos(conexion, query);
            resultado   =   true;
        }catch(Exception e){
            resultado   =   false;
        }
        
        return resultado;
    }
    
    public void cerrarConexion(){

     try {
         bdInterfaz.desconectarBaseDatos(getConexion());
     } catch (Exception e) {}
     
    }
    
    /****************/
    public Connection getConexion() {
        return conexion;
    }

    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }
    
}
