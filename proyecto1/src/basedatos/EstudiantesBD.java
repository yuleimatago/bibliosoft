/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basedatos;

import clases.BaseDatos;
import java.sql.Connection;
import java.sql.ResultSet;

/**
 *
 * @author rorti
 */
public class EstudiantesBD {
    private BaseDatos   bdInterfaz;
    private Connection conexion;
    
    public EstudiantesBD() throws Exception{
        this.bdInterfaz =   new BaseDatos();
        this.conexion   =   bdInterfaz.conectarBaseDatos();
    }
    
    public EstudiantesBD(Connection conexion) {
        this.bdInterfaz =   new BaseDatos();
        this.conexion   =   conexion;
    }
    
    public String[]  consultarEstudiante(String identificacion){
        ResultSet   resultSet   =   null;
        String[]    estudiantes =   new String[6];
        String query = "SELECT * FROM estudiantes WHERE identificacion = '"+identificacion+"' LIMIT 1";
        
        try{
            resultSet   =   bdInterfaz.consultaDinamicaRegistrosBaseDatos(getConexion(), query);
            
            if (resultSet.next()) {
                
                estudiantes[0]  =   resultSet.getInt("id")+"";
                estudiantes[1]  =   resultSet.getString("identificacion");
                estudiantes[2]  =   resultSet.getString("nombre");
                estudiantes[3]  =   resultSet.getString("apellido");
                estudiantes[4]  =   resultSet.getString("grado");
                estudiantes[5]  =   resultSet.getString("numeroContacto");
            }else{
                estudiantes =   null;
            }
            
        }catch(Exception e){
            estudiantes =   null;
        }
        
        return estudiantes;
    }
    
    public boolean  insertarEstudiante(String[] estudiante){
        boolean resultado   =   false;
        String query = "INSERT INTO estudiantes "
                + "(identificacion, nombre, apellido, grado, numeroContacto) " 
                + " VALUES ('"+estudiante[1]+"'"
                + ",'"+estudiante[2]+"'"
                + ",'"+estudiante[3]+"'"
                + ",'"+estudiante[4]+"'"
                + ",'"+estudiante[5]+"') ";        
        try{
            bdInterfaz.insertarRegistroBaseDatos(conexion, query);
            resultado   =   true;
        }catch(Exception e){
            resultado   =   false;
        }
        
        return resultado;
    }
    
    public boolean  actualizarEstudiante(String[] estudiante){
        boolean resultado   =   false;
        String query = "UPDATE estudiantes SET "
                + "identificacion = '"+estudiante[1]+"'"
                + ", nombre = '"+estudiante[2]+"'"
                + ", apellido = '"+estudiante[3]+"'"
                + ", grado = '"+estudiante[4]+"'"
                + ", numeroContacto = '"+estudiante[5]+"'"
                + "WHERE id = "+estudiante[0];
        
        try{
            bdInterfaz.insertarRegistroBaseDatos(conexion, query);
            resultado   =   true;
            
        }catch(Exception e){
            resultado   =   false;
        }
        
        return resultado;
    }
    
    public void cerrarConexion(){

     try {
         bdInterfaz.desconectarBaseDatos(getConexion());
     } catch (Exception e) {}
     
    }
    
    /****************/
    public Connection getConexion() {
        return conexion;
    }

    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }
    
}
