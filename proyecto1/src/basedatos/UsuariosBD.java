/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basedatos;

import clases.BaseDatos;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author rorti
 */
public class UsuariosBD {
    private BaseDatos   bdInterfaz;
    private Connection conexion;
    
    public UsuariosBD() throws Exception{
        this.bdInterfaz =   new BaseDatos();
        this.conexion   =   bdInterfaz.conectarBaseDatos();
    }
    
    public UsuariosBD(Connection conexion) {
        this.bdInterfaz =   new BaseDatos();
        this.conexion   =   conexion;
    }
    
    public String[] consultarUsuarioTipo(String identificacion,String tipo){
        ResultSet   resultSet   =   null;
        String[]    estudiantes =   new String[9];
        String query = "SELECT id, identificacion, usuario, contrasena, tipoUsuario, nombres, apellidos, estado , numeroContacto"
                     + " FROM usuarios "
                     + " WHERE identificacion = '"+identificacion+"' "
                     + " AND tipoUsuario = '"+tipo+"' "
                     + " LIMIT 1";
        
        try{
            resultSet   =   bdInterfaz.consultaDinamicaRegistrosBaseDatos(getConexion(), query);
            
            if (resultSet.next()) {
                
                estudiantes[0]  =   resultSet.getInt("id")+"";
                estudiantes[1]  =   resultSet.getString("identificacion");
                estudiantes[2]  =   resultSet.getString("usuario");
                estudiantes[3]  =   resultSet.getString("contrasena");
                estudiantes[4]  =   resultSet.getString("tipoUsuario");
                estudiantes[5]  =   resultSet.getString("nombres");
                estudiantes[6]  =   resultSet.getString("apellidos");
                estudiantes[7]  =   resultSet.getString("estado");
                estudiantes[8]  =   resultSet.getString("numeroContacto");
            }else{
                estudiantes =   null;
            }
            
        }catch(Exception e){
            estudiantes =   null;
        }
        
        return estudiantes;
    }
    
    public boolean  insertarUsuario(String[] usuario){
        boolean resultado   =   false;
        String query = "INSERT INTO usuarios "
                +"  (identificacion,usuario,contrasena,tipoUsuario,nombres,apellidos,numeroContacto)"
                + " VALUES ('"+usuario[1]+"'"
                + ",'"+usuario[2]+"'"
                + ",'"+usuario[3]+"'"
                + ",'"+usuario[4]+"'"
                + ",'"+usuario[5]+"'"
                + ",'"+usuario[6]+"'"
                + ",'"+usuario[7]+"') ";        
        try{
            bdInterfaz.insertarRegistroBaseDatos(conexion, query);
            resultado   =   true;
        }catch(Exception e){
            resultado   =   false;
        }
        
        return resultado;
    }
    
    public boolean  actualizarUsuario(String[] usuario){
        boolean resultado   =   false;
        String query = "UPDATE usuarios SET "
                + "identificacion = '"+usuario[1]+"'"
                + ",usuario = '"+usuario[2]+"'"
                + ",contrasena= '"+usuario[3]+"'"
                + ",tipoUsuario = '"+usuario[4]+"'"
                + ",nombres= '"+usuario[5]+"'"
                + ",apellidos= '"+usuario[6]+"'"
                + ",numeroContacto= '"+usuario[7]+"' "
                + " WHERE id = "+usuario[0];
        
        try{
            bdInterfaz.insertarRegistroBaseDatos(conexion, query);
            resultado   =   true;
            
        }catch(Exception e){
            resultado   =   false;
        }
        
        return resultado;
    }
        
        
    public String[] consultarUsuario(String identificacion){
        ResultSet   resultSet   =   null;
        String[]    estudiantes =   new String[9];
        String query = "SELECT id, identificacion, usuario, contrasena, tipoUsuario, nombres, apellidos, estado ,numeroContacto"
                     + " FROM usuarios "
                     + " WHERE identificacion = '"+identificacion+"' "
                     + " LIMIT 1";
        
        try{
            resultSet   =   bdInterfaz.consultaDinamicaRegistrosBaseDatos(getConexion(), query);
            
            if (resultSet.next()) {
                
                estudiantes[0]  =   resultSet.getInt("id")+"";
                estudiantes[1]  =   resultSet.getString("identificacion");
                estudiantes[2]  =   resultSet.getString("usuario");
                estudiantes[3]  =   resultSet.getString("contrasena");
                estudiantes[4]  =   resultSet.getString("tipoUsuario");
                estudiantes[5]  =   resultSet.getString("nombres");
                estudiantes[6]  =   resultSet.getString("apellidos");
                estudiantes[7]  =   resultSet.getString("estado");
                estudiantes[8]  =   resultSet.getString("numeroContacto");
            }else{
                estudiantes =   null;
            }
            
        }catch(Exception e){
            estudiantes =   null;
        }
        
        return estudiantes;
    }
    
    public boolean  verificarUsuario(String usuario,String contrasena){
        boolean resultado   =   false;
        ResultSet   resultSet   =   null;
        String query = "SELECT * FROM usuarios WHERE usuario = '"+usuario+"'  AND contrasena = '"+contrasena+"'";
        
        try{
            resultSet   =   bdInterfaz.consultaDinamicaRegistrosBaseDatos(getConexion(), query);
            
            if (resultSet.next()) {
                resultado   =   true;
            }else{
                resultado   =   false;
            }
        }catch(Exception e){
            resultado   =   false;
        }
        
        return resultado;
    }
    
    public String[] obtenerUsuario(String usuario){
        String[]    resultado   =   null;
        ResultSet   resultSet   =   null;
        String query = "SELECT * FROM usuarios WHERE usuario = '"+usuario+"' LIMIT 1";
        
        try{
            resultSet   =   bdInterfaz.consultaDinamicaRegistrosBaseDatos(getConexion(), query);
            if (resultSet.next()) {
                resultado   =   new String[7];
                resultado[0]    =   resultSet.getString("id");
                resultado[1]    =   resultSet.getString("usuario");
                resultado[2]    =   resultSet.getString("contrasena");
                resultado[3]    =   resultSet.getString("tipoUsuario");
                resultado[4]    =   resultSet.getString("nombres");
                resultado[5]    =   resultSet.getString("apellidos");
                resultado[5]    =   resultSet.getString("estado");
                  
            }else{
                resultado   =   null;
            }
        }catch(Exception e){
            resultado   =   null;
        }
      
        
        return resultado;
    }
    
    public boolean  actualizarContrasena(String idUsuario,String contrasena){
        boolean resultado   =   false;
        String query = "UPDATE usuarios SET "
                + "contrasena = '"+contrasena+"' "
                + "WHERE id = '"+idUsuario+"' ";
        
        try{
            bdInterfaz.insertarRegistroBaseDatos(conexion, query);
            resultado   =   true;
            
        }catch(Exception e){
            resultado   =   false;
        }
        
        return resultado;
    }
    
    public void cerrarConexion(){

     try {
         bdInterfaz.desconectarBaseDatos(getConexion());
     } catch (Exception e) {}
     
    }
    
    /****************/
    public Connection getConexion() {
        return conexion;
    }

    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }
    
}
