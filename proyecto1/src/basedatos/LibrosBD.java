/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basedatos;

import clases.BaseDatos;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author rorti
 */
public class LibrosBD {
    private BaseDatos   bdInterfaz;
    private Connection conexion;
    
    public LibrosBD() throws Exception{
        this.bdInterfaz =   new BaseDatos();
        this.conexion   =   bdInterfaz.conectarBaseDatos();
    }
    
    public LibrosBD(Connection conexion) {
        this.bdInterfaz =   new BaseDatos();
        this.conexion   =   conexion;
    }
    
    public ArrayList<String[]>  consultarLibros(){
        ArrayList<String[]> resultado = new ArrayList<>();
        ResultSet   resultSet   =   null;
        String query = "SELECT * FROM books ";
        
        try{
            resultSet   =   bdInterfaz.consultaDinamicaRegistrosBaseDatos(getConexion(), query);
            
            while(resultSet.next()) {
                String[]    libro =   new String[9];
                libro[0] =	resultSet.getString("id");
                libro[1] =	resultSet.getString("cod");
                libro[2] =	resultSet.getString("title");
                libro[3] =	resultSet.getString("author");
                libro[4] =	resultSet.getString("gender");
                libro[5] =	resultSet.getString("quantity");
                libro[6] =	resultSet.getString("editorial");
                libro[7] =	resultSet.getString("shelf");
                libro[8] =	resultSet.getString("sub_shelft");
                
                resultado.add(libro);
            }
            
        }catch(Exception e){
        }
        
        return resultado;
    }
        
    public String[]  consultarLibro(String isbn){
        ResultSet   resultSet   =   null;
        String[]    libro =   new String[9];
        String query = "SELECT * FROM books WHERE cod = '"+isbn+"' LIMIT 1";
        
        try{
            resultSet   =   bdInterfaz.consultaDinamicaRegistrosBaseDatos(getConexion(), query);
            
            if (resultSet.next()) {
                libro[0] =	resultSet.getString("id");
                libro[1] =	resultSet.getString("cod");
                libro[2] =	resultSet.getString("title");
                libro[3] =	resultSet.getString("author");
                libro[4] =	resultSet.getString("gender");
                libro[5] =	resultSet.getString("quantity");
                libro[6] =	resultSet.getString("editorial");
                libro[7] =	resultSet.getString("shelf");
                libro[8] =	resultSet.getString("sub_shelft");
            }else{
                libro =   null;
            }
            
        }catch(Exception e){
            System.out.println(e.getMessage());
            libro =   null;
        }
        
        return libro;
    }
    
    public boolean actualizarNumeroLibros(int idLibro,int numeroLibros){
        boolean resultado   =   false;
        String query = "UPDATE books SET "
                + " , quantity = '"+numeroLibros+"' "
                + "WHERE id = "+idLibro;
        
        try{
            bdInterfaz.insertarRegistroBaseDatos(conexion, query);
            resultado   =   true;
            
        }catch(Exception e){
            resultado   =   false;
        }
        
        return resultado;
    }
    
    public boolean  actualizarLibro(String[] libro){
        boolean resultado   =   false;
        String query = "UPDATE books SET "
                + "   cod = '"+libro[1]+"' "
                + " , title = '"+libro[2]+"' "
                + " , author = '"+libro[3]+"' "
                + " , gender = '"+libro[4]+"' "
                + " , quantity = '"+libro[5]+"' "
                + " , editorial = '"+libro[6]+"' "
                + " , shelf = '"+libro[7]+"' "
                + " , sub_shelft = '"+libro[8]+"' "
                + "WHERE id = "+libro[0];
        
        try{
            bdInterfaz.insertarRegistroBaseDatos(conexion, query);
            resultado   =   true;
            
        }catch(Exception e){
            resultado   =   false;
        }
        
        return resultado;
    }
    
        
    public boolean  insertarLibro(String[] libro){
        boolean resultado   =   false;
        String query = "INSERT INTO books "
                + "( cod, title, author, gender, quantity, editorial, shelf, sub_shelft ) " 
                + " VALUES ("
                + "'"+libro[1]+"', "
                + "'"+libro[2]+"', "
                + "'"+libro[3]+"', "
                + "'"+libro[4]+"', "
                + "'"+libro[5]+"', "
                + "'"+libro[6]+"', "
                + "'"+libro[7]+"', "
                + "'"+libro[8]+"' "
                + ") ";        
        try{
            bdInterfaz.insertarRegistroBaseDatos(conexion, query);
            resultado   =   true;
        }catch(Exception e){
            resultado   =   false;
        }
        
        return resultado;
    }
    
    public void cerrarConexion(){

     try {
         bdInterfaz.desconectarBaseDatos(getConexion());
     } catch (Exception e) {}
     
    }
    
    /****************/
    public Connection getConexion() {
        return conexion;
    }

    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }
    
}
