/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyecto1;

import basedatos.LibrosBD;
import clases.ExcelHelper;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;

/**
 *
 * @author rorti
 */
public class SubidaLibros extends javax.swing.JFrame {
    
    private File    archivoSeleccionado;
    private boolean administracion;
    /**
     * Creates new form SubidaLibros
     */
    public SubidaLibros() {
        initComponents();
        setResizable(false);
        setTitle("MENU - BibliotecaSoft COLSAM.");
        ImageIcon icon= new ImageIcon ("src//images//icono.png");
             this.setIconImage(icon.getImage());
    }
    
    public SubidaLibros(boolean admin) {
        initComponents();
        setResizable(false);
        setTitle("MENU - BibliotecaSoft COLSAM.");
        ImageIcon icon= new ImageIcon ("src//images//icono.png");
             this.setIconImage(icon.getImage());
        this.administracion =   admin;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        lbNombreArchivo = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jButton1.setText("Seleccionar archivo");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        lbNombreArchivo.setText("Nada subido aún");

        jButton2.setText("Subir datos");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(53, 53, 53)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton2)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jButton1)
                        .addComponent(lbNombreArchivo, javax.swing.GroupLayout.PREFERRED_SIZE, 303, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(44, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(56, 56, 56)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbNombreArchivo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 111, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addGap(67, 67, 67))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        JFileChooser chooser    =   new JFileChooser();
        chooser.showOpenDialog(null);
        archivoSeleccionado =   chooser.getSelectedFile();
        String ruta = archivoSeleccionado.getAbsolutePath();
        lbNombreArchivo.setText(ruta);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
         try {
            ExcelHelper eh  =   new ExcelHelper(archivoSeleccionado);
            ArrayList<String[]> filas   =   eh.read();
            for (String[] fila : filas) {
                if (fila[0] != "") {
                    LibrosBD   bd = null;
                    String[]    libro  =   new String[(fila.length + 1)];
                    int ite =   0;
                    libro[0]   =   "0";
                    for (int i = 1; i < libro.length ; i++) {
                        libro[i]   =   fila[ite];
                        ite++;
                    }
//                  for (int j = 0; j < estudiante.length; j++) {
//                      System.out.println(estudiante[j]);
//                  }
//                  System.out.println("-");                    
                    try {
                        bd  =   new LibrosBD();

                        String[] consulta =    bd.consultarLibro(libro[1]);    
                        if (consulta != null) {
                            bd.actualizarLibro(libro);
                        }else{
                            bd.insertarLibro(libro);
                        }
                    } catch (Exception ex) {
                        System.out.println("excepción "+ex.getMessage());
                    } finally{
                        if (bd != null) {
                            bd.cerrarConexion();
                            bd  =   null;
                        }
                    }
                }

            }

        } catch (IOException ex) {
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(SubidaLibros.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(SubidaLibros.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(SubidaLibros.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(SubidaLibros.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new SubidaLibros().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel lbNombreArchivo;
    // End of variables declaration//GEN-END:variables
}
