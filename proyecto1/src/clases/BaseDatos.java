/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author rorti
 */
public class BaseDatos {
      public BaseDatos() {
    }
    
    public Connection conectarBaseDatos() throws Exception{

        Connection conn = null;
        String msgError =   "";
        
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            conn = DriverManager.getConnection("jdbc:mysql://localhost/bibliotecasoftv2","root","");  
        } catch ( SQLException 
                | ClassNotFoundException 
                | IllegalAccessException 
                | InstantiationException e) {
            
                msgError = "Error!! problemas en la conexión a la base de datos \n"+ e.getMessage();
                
                throw new Exception(msgError, e.getCause());
        }

        return conn;
    }

    public void desconectarBaseDatos(Connection conexion) throws Exception {
        
        String msgError;
        
        if (conexion != null) {
            try {
                conexion.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                msgError = "Error!! problemas para desconectar la base de datos.\n" + e.getMessage();
                throw new Exception(msgError, e.getCause());
            }
        }
    }
    
    public ResultSet consultaDinamicaRegistrosBaseDatos(Connection conexion
                                                        , String consulta
                                                       ) throws Exception {

        String sql;
        PreparedStatement consultaSql;
        ResultSet resultados = null;
        String msgErrorLog;
        

        try {
            
            System.out.println(consulta);
            
            consultaSql = conexion.prepareStatement(consulta);
            resultados = consultaSql.executeQuery();
            
            
        } catch (SQLException e) {
            msgErrorLog = "Error!! consultando en la base de datos \n " + e.getMessage();
            throw new Exception(msgErrorLog);
        }

        return resultados;
    }
    
    public int insertarRegistroBaseDatos(Connection conexion
                                          , String sql
                                          ) throws Exception {
        
        int resultado   =   0;
        String msgError;
        String msgErrorLog;
        PreparedStatement comandoSql=null;
        
        try {
            System.out.println(sql);
            comandoSql = conexion.prepareStatement(sql);
            resultado   =   comandoSql.executeUpdate();
        } catch (SQLException e) {
            msgError = "Error!! Insertando el registro en la base de datos";
            throw new Exception(msgError, e.getCause());
        } finally {
            try {
                if (comandoSql != null) {
                    comandoSql.close();
                }
            } catch (SQLException e) {
                msgError = "Error!! Insertando el registro en la base de datos.";
                throw new Exception(msgError, e.getCause());
            }
        }
        
        return resultado;
    }
    
    public int insertarRegistroBaseDatosConId(Connection conexion
                                          , String sql
                                          ) throws Exception {
        
        ResultSet   result = null;
        int resultado   =   0;
        String msgError;
        String msgErrorLog;
        PreparedStatement comandoSql=null;
        
        try {
            System.out.println(sql);
            comandoSql = conexion.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
            comandoSql.executeUpdate();
            
            result  =   comandoSql.getGeneratedKeys();
            
            if (result.next()) {
                resultado   =   result.getInt(1);
            }
        } catch (SQLException e) {
            msgError = "Error!! Insertando el registro en la base de datos";
            throw new Exception(msgError, e.getCause());
        } finally {
            try {
                if (comandoSql != null) {
                    comandoSql.close();
                }
            } catch (SQLException e) {
                msgError = "Error!! Insertando el registro en la base de datos.";
                throw new Exception(msgError, e.getCause());
            }
        }
        
        return resultado;
    }
      
}
