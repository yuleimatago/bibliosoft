/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 *
 * @author rorti
 */
public class Helpers {
       
    public Date sumarDiasAFecha(Date fecha, int dias) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha); // Configuramos la fecha que se recibe
        calendar.add(Calendar.DAY_OF_YEAR, dias);  // numero de días a añadir, o restar en caso de días<0
        return calendar.getTime(); // Devuelve el objeto Date con los nuevos días añadidos
    }
    
    public String obtenerFechaDB(Date fecha) {
        String mesActual;
        String diaActual;
        try {
            Calendar calendario = new GregorianCalendar();
            calendario.setTime(fecha);

            int mes = (calendario.get(Calendar.MONTH) + 1);
            int dia =  (calendario.get(Calendar.DAY_OF_MONTH)); 
            
            if (mes < 10) {
                mesActual = "0" + mes;
            } else {
                mesActual = Integer.toString(mes);
            }
            
            if (dia < 10) {
                diaActual = "0" + dia;
            } else {
                diaActual = dia + "";
            }

            return "'" + calendario.get(Calendar.YEAR) + "-" + mesActual + "-" +diaActual + "'";
        } catch (Exception e) {
            return "null";
        }
    }
    
    public String obtenerFechaTXT(Date fecha) {
        String mesActual;
        String diaActual;
        try {
            Calendar calendario = new GregorianCalendar();
            calendario.setTime(fecha);

            int mes = (calendario.get(Calendar.MONTH) + 1);
            int dia =  (calendario.get(Calendar.DAY_OF_MONTH)); 
            
            if (mes < 10) {
                mesActual = "0" + mes;
            } else {
                mesActual = Integer.toString(mes);
            }
            
            if (dia < 10) {
                diaActual = "0" + dia;
            } else {
                diaActual = dia + "";
            }

            return calendario.get(Calendar.YEAR) + "-" + mesActual + "-" +diaActual;
        } catch (Exception e) {
            return "null";
        }
    }
    
    public Date convertirTextoAFecha(String fechatxt){
        
        Date fecha = null;
        String valor=null;
        SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd");
        
        try{
            if(fechatxt != null){
                fecha = formatoFecha.parse(fechatxt);
            }else{
                fecha = null;
            }
           
            
        }catch(ParseException ex){
            valor = null;
        }
        
        return fecha;
    }
    
}
