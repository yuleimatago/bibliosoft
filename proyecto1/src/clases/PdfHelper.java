/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;
import java.io.FileOutputStream;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.IOException;
import java.util.ArrayList;

public class  PdfHelper{

    public void crear(String nombre,ArrayList<String[]> listado)throws IOException, DocumentException{
         new PdfHelper().createPdf(nombre,listado);
    }
 
    /**
     * Creates a PDF with information about the movies
     * @param    filename the name of the PDF file that will be created.
     * @throws    DocumentException 
     * @throws    IOException
     */
    public void createPdf(String filename,ArrayList<String[]>listado)
        throws IOException, DocumentException {
    	// step 1
        Document document = new Document();
        // step 2
        PdfWriter.getInstance(document, new FileOutputStream(filename));
        // step 3
        document.open();
        // step 4
        document.add(createFirstTable(listado));
        // step 5
        document.close();
    }
 
    /**
     * Creates our first table
     * @return our first table
     */
    public static PdfPTable createFirstTable(ArrayList<String[]>listado) {
    	// a table with three columns

        PdfPTable table;
        if (listado.size() > 0) {
            table = new PdfPTable(listado.get(0).length);
            
            for (String[] valores : listado) {
                for (int i = 0; i < valores.length; i++) {
                    table.addCell(valores[i]);
                }
            }
        }else{
            table = new PdfPTable(1);
        }
        return table;
    }
}

