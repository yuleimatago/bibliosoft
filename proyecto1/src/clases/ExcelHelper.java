/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

/**
 *
 * @author rorti
 */
public class ExcelHelper {
    private File    inputWorkbook;
    
    public ExcelHelper(File inputWorkbook) {
        this.inputWorkbook =   inputWorkbook;
    }
    
    public ExcelHelper() {
    }
    
    public void write(String nombreArchivo,ArrayList<String[]> listaInformacion){
        try {
            FileOutputStream fileOut = new FileOutputStream(nombreArchivo);
            HSSFWorkbook workbook = new HSSFWorkbook();
            HSSFSheet worksheet = workbook.createSheet("POI Worksheet");
            
            int poscFila = 0;
            for(String[] datos : listaInformacion){
                HSSFRow fila = worksheet.createRow((short) poscFila);
                for (int i = 0; i < datos.length; i++) {
                    HSSFCell celda = fila.createCell((short) i);
                    celda.setCellValue(datos[i]);
                }
                poscFila++;
            }
            workbook.write(fileOut);
            fileOut.flush();
            fileOut.close();
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
                e.printStackTrace();
        } catch (IOException e) {
                e.printStackTrace();
        }

    }
    
    public ArrayList<String[]> read() throws IOException  {
        ArrayList<String[]> resultado   =   new ArrayList<>();
        try {
            int numeroFilas =   0;
            FileInputStream excelFile = new FileInputStream(getArchivo());
            Workbook workbook = new HSSFWorkbook(excelFile);
            Sheet datatypeSheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = datatypeSheet.iterator();

            while (iterator.hasNext()) {
                
                Row currentRow = iterator.next();
                Iterator<Cell> cellIterator = currentRow.iterator();
                String[]    fila    =   new String[currentRow.getPhysicalNumberOfCells()];
                int conteo  =   0;
                while (cellIterator.hasNext()) {
                    Cell currentCell = cellIterator.next();
                    String valor    =   "";
//                    System.out.println(currentCell);
                    if (currentCell.getCellType() == Cell.CELL_TYPE_STRING) {
                        valor   =   currentCell.getStringCellValue();
                    } else if (currentCell.getCellType() == Cell.CELL_TYPE_NUMERIC) {
                        DataFormatter fmt = new DataFormatter();
//                        System.out.println(fmt.formatCellValue(currentCell));
//                        valor   =   Double.valueOf(currentCell.getNumericCellValue() + "")+"";
                        valor   =   fmt.formatCellValue(currentCell);
                    }
                    fila[conteo]    =   valor;
                    conteo++;
                }
                
//                resultado.add(fila);
//            while (iterator.hasNext()) {
//                
//                HSSFRow row = (HSSFRow) datatypeSheet.getRow(numeroFilas); 
//                
//                if(row !=null){
//                    
//                    for(int indice=0; indice< row.getPhysicalNumberOfCells(); indice++)  {  
//                        System.out.println(row.getCell(indice));
//                          //Se obtiene la celda i-esima   
////                        HSSFCell celda = row.getCell(indice);   
//                    }
////                    if(data.size() == numeroColumnas ){
////                            listaDatos.add(data);
////                    }
//                }else{
//                    break;
//                }
//                
//                numeroFilas++;
//            }
                resultado.add(fila);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return resultado;
    }

    public File getArchivo() {
        return inputWorkbook;
    }

    public void setArchivo(File archivo) {
        this.inputWorkbook = archivo;
    }
}
